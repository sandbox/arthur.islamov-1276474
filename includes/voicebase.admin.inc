<?php
/**
 * @file
 * Admin settings for voicebase module
 */
/**
 * Implements hook_settings_form().
 */
function voicebase_settings_form($form, &$form_state) {
  $form = array();
  
  $check = voicebase_chek_nodes();
  
  
  
  if ((variable_get('voicebase_api') != "") && (variable_get('voicebase_pass') != "")) {
    $main_collapsed = TRUE;
  } else {
    $main_collapsed = FALSE;  
  }
  
  $form['nodes'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Check nodes'), 
    '#weight' => 0, 
    '#collapsible' => FALSE, 
    '#collapsed' => FALSE,
  );
  
//  $form['nodes']['voicebase_select action'] = array(
//    '#type' => 'checkbox',
//    '#title' => t("Check VoiceBase for transcription updates"),
//    '#default_value' => false,
//    '#value' => false,
//    '#description' => t('Check this and save settings to load transription updates'),
//  );
  
  if ($check["empty"] == 0) {
    drupal_set_message(t("All your VoiceBase nodes are transcripted"), "status", FALSE);
  } else {
    drupal_set_message(t("You have {$check["empty"]} untranscripted recording(s)"), "warning", FALSE);
    $form['nodes']['voicebase_chek_for_updates'] = array(
      '#type' => 'submit',
      '#value' => t('Check VoiceBase.com for transcription updates'),
      '#ajax' => array(
        'callback' => 'voicebase_ajax_check_for_updates',
        'wrapper' => 'voicebase_check_for_updates',
        'method' => 'replace',
        'effect' => 'slide',
      ),
      "#suffix" => "<div id='voicebase_check_for_updates'></div>",
    );
  }
  
  
  $form['main'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Main settings'), 
    '#weight' => 1, 
    '#collapsible' => TRUE, 
    '#collapsed' => $main_collapsed,
  );
  
  $form['main']['voicebase_api'] = array(
    '#type' => 'textfield',
    '#title' => t("VoiceBase API key"),
    '#default_value' => variable_get('voicebase_api', ''),
    '#description' => t('Specify your VoiceBase API key'),
  );
  
  $form['main']['voicebase_pass'] = array(
    '#type' => 'textfield',
    '#title' => t("VoiceBase Password"),
    '#default_value' => variable_get('voicebase_pass', ''),
    '#description' => t('Specify your VoiceBase account password'),
  );
  
  $form['fields_settings'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Fields Settings'), 
    '#weight' => 3, 
    '#collapsible' => TRUE, 
    '#collapsed' => !$main_collapsed,
  );

  $types = node_type_get_types();
  $options = array();
  $is_inst_set = array();
  $requaired_fields = array("field_vb_record", "field_vb_keywords", "field_vb_mediaid");
  foreach($types as $key => $type) {
    $arraymsg = array(); 
    foreach ($requaired_fields as $rf) {
      $is_inst_set = field_read_instance("node", $rf, $key);
      if ($is_inst_set) {
        $arraymsg[] = $rf;    
      }  
    }
    switch (count($arraymsg)) {
      case 1:
        $e_style = 'class="vbcred"';
        break;
      case 2:
        $e_style = 'class="vbcred"';
        break;
      case 3:
        $e_style = 'class="vbcgreen"';
        break;
      case 0 :
        $e_style = "";  
        break;   
    }
    if (!empty($arraymsg)) {
      $textmsg = implode(",",$arraymsg);
      $textmsg = "<span " . $e_style . "> (" . $textmsg . ")</span>";
    } else {
      $textmsg = "";
    }
    $options[$key] = $type->name;
    unset($textmsg);
    unset($is_inst_set);
    unset($e_style);
    unset($arraymsg);
    
  }
  
  $enabled_types = variable_get('voicebase_node_types', '');
  
  $form['fields_settings']['voicebase_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#default_value' => $enabled_types,
    '#options' => $options,
    '#description' => t('Check content types to use with VoiceBase. All necessary fields will be created automatically.'),
  );
  
  if ((is_array ($enabled_types)) && (!empty($enabled_types))) {
    $form['fields_settings']['voicebase_media_fields'] = array(
      '#type' => 'fieldset', 
      '#title' => t('Media Fields'), 
      '#weight' => 10, 
      '#collapsible' => TRUE, 
      '#collapsed' => FALSE,
    );
    foreach ($enabled_types as $ctype => $e_type) {
      if (!empty($e_type)) {
        $fields = field_info_instances("node", $ctype); 
        $options = array("none" => "Select field");
        foreach ($fields as $key => $field) {
          if (substr_count($key, "field")) {
            $options[$key] = $field["label"] . " ($key)";
          }  
        }
        
        $form['fields_settings']['voicebase_media_fields']["voicebase_".$e_type."_media_field"] = array(
          '#type' => 'select',
          '#title' => t("Media Field for \"".$e_type."\""),
          '#options' => $options,
          '#default_value' => variable_get("voicebase_".$e_type."_media_field", 'none'),
          '#description' => t('Select field for media files'),
        );
        
        if (variable_get("voicebase_".$e_type."_media_field", 'none') == "none") {
          drupal_set_message(t("<strong>Do not forget to select media field for \"$e_type\" content type</strong>"), "warning", false);
        }  
      }
    }  
  }
  
  $form['#validate'][] = 'voicebase_settings_form_validate';
  $form['#submit'][] = 'voicebase_settings_form_submit';
  
  return system_settings_form($form);
}

/**
 * Implements hook_settings_form_validate().
 */
function voicebase_settings_form_validate($form, &$form_state) {
  if ($form_state['values']['voicebase_api'] == "") {
    form_error($form['main']['voicebase_api'], t('You need to provide your VoiceBase API key'));
  }
  if ($form_state['values']['voicebase_pass'] == "") {
    form_error($form['main']['voicebase_pass'], t('You need to provide your VoiceBase password'));
  }
}

/**
 * Callback function for ajax request.
 */
function voicebase_ajax_check_for_updates ($form, &$form_state) {
  $result = voicebase_load_transcript (); 

  return $result;   
}

/**
 * Implements hook_settings_form_submit().
 */
function voicebase_settings_form_submit($form, &$form_state) {
  foreach ($form_state["values"]["voicebase_node_types"] as $node_type) {
    if (!empty($node_type)) {
      voicebase_create_fields($node_type);  
    }
  } 
}

/**
 * Check count of untranscripted nodes.
 */
function voicebase_chek_nodes () {
  $count = 0;
  $results = db_query("SELECT bundle FROM {field_config_instance} WHERE field_name = 'field_vb_keywords'")->fetchCol();
  $results = array_unique($results);
  foreach ($results as $result) {
    $nodes = node_load_multiple(db_query("SELECT nid FROM {node} n INNER JOIN {field_data_field_vb_mediaid} fm ON fm.entity_id = n.nid WHERE n.type = '$result' AND fm.field_vb_mediaid_value != ''")->fetchCol());
    if (!empty($nodes)) {
      foreach($nodes as $node) {
        if (empty($node->field_vb_keywords)) {
          $count = $count + 1;  
        }
      }  
    }  
  }
  return array("total" => count($nodes), "empty" => $count);
}

/**
 * Load transcription from voicebase.com.
 */
function voicebase_load_transcript () {
  $results = db_query("SELECT bundle FROM {field_config_instance} WHERE field_name = 'field_vb_keywords'")->fetchCol();
  $results = array_unique($results);
  $counter = 0;
  foreach ($results as $result) {
    $nodes = node_load_multiple(db_query("SELECT nid FROM {node} n INNER JOIN {field_data_field_vb_mediaid} fm ON fm.entity_id = n.nid WHERE n.type = '$result' AND fm.field_vb_mediaid_value != ''")->fetchCol());
    if (!empty($nodes)) {
      foreach($nodes as $node) {
        if (empty($node->field_vb_keywords)) {
          $mediaid = $node->field_vb_mediaid["und"][0]["value"];
          $vb = voicebase_get_trascript ($mediaid);
          $vb = json_decode($vb);
          
          if ($vb) {
            if ($vb->requestStatus == "SUCCESS") {
              if ($vb->fileStatus == "COMPLETE") {
                if (($vb->keywords != "") && ($vb->keywords != "null")) {
                  
                  $node->field_vb_keywords["und"][0]["value"] = json_encode($vb->keywords);  
                  node_save($node);  
                }
                 
              }
            }  
          }   
        }
      }  
    }
  }
  
  return "Checked successfully. $counter transcription(s) was loaded.";
}

/**
 * Create necessary fields.
 */
function voicebase_create_fields ($node_type) {
 
  //-------------------------  field_vb_keywords  ------------------------------
  $is_inst_set = field_read_instance("node", "field_vb_keywords", $node_type);
  
  if (!$is_inst_set) {
    $instance = array(
      'field_name' => "field_vb_keywords",
      'entity_type' => 'node',
      'bundle' => $node_type,
      'label' => t('Transcription'),
      'description' => t('Field for VoiceBase transcription.'),
      'widget' => array(
        'type' => 'text_long',
        'weight' => 10,
      ),
    );
    
    field_create_instance($instance);
  } else {
//    drupal_set_message(t("Field field_vb_keywords is already exist in $node_type"), "warning", false);
  }  
  
  //-------------------------  field_vb_record  ------------------------------
  $is_inst_set = field_read_instance("node", "field_vb_record", $node_type);
  
  if (!$is_inst_set) {
//    $field = array(
//      'field_name' => 'field_vb_record',
//      'type' => 'text',
//      'cardinality' => 1, 
//    );
//    field_create_field($field);
    
    $instance = array(
        'field_name' => 'field_vb_record',
        'label'       => t("Recording"),
        'bundle' => $node_type,
        'entity_type' => 'node',
        'widget'      => array(
          'type'    => 'file_generic',
        ),
        'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'voicebase_player',
          ),
        ),
        'settings' => array(
          'file_extensions' => 'mp3 flv',
        ),
        'description' => 'Recording for transcript',
      );
    
    field_create_instance($instance);
  }
  $is_inst_set = field_read_instance("node", "field_vb_mediaid", $node_type);
  
  if (!$is_inst_set) {
    $instance = array(
      'field_name' => 'field_vb_mediaid',
      'entity_type' => 'node',
      'bundle' => $node_type,
      'label' => t('MediaID'),
      'description' => t('You can enter your last name here.'),
      'widget' => array(
        'type' => 'text_textfield',
        'weight' => 10,
      ),
    );
    
    field_create_instance($instance);
  } else {
  } 
  
}
