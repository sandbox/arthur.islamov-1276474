<?php

/**
 * @file
 * User page callbacks for the voicebase module.
 */

/*
 * Presents links to filter the search results.
 */
function voicebase_preprocess_search_results(&$variables) {
  $variables['filter_position'] = 'disabled';
  $GLOBALS['custom_search_nb_results'] = count($variables['results']);
  // generate the filter
  if (user_access('use custom search') && $variables['filter_position'] != 'disabled') {
    // Get search words (minus type:node_type)
    $path = explode('/', $_GET['q'], 3);
    $keys = empty($_REQUEST['keys']) ? '' : $_REQUEST['keys'];
    if (count($path) == 3) $keys = $path[2];
    if (strpos($keys, 'type:') !== FALSE) {
      $keys = drupal_substr($keys, 0, strpos($keys, 'type:')-1);
    }
    $variables['keys'] = $keys; 
    $searchable_node_types = array();
    $searchable_node_types = array_keys(array_filter($searchable_node_types, 'voicebase_filter_array'));
    if (!count($searchable_node_types)) $searchable_node_types = array_keys(node_type_get_names());
    $node_types = db_query("SELECT type, name FROM {node_type} WHERE type IN (:ntypes)", array(':ntypes' => $searchable_node_types));
    // Build menu
    $items = array();
    $items[] = l( 'search/node/' . $keys);
    foreach ($node_types as $node_type) {
      // count # of results per type
      $nbresults = 0;
      foreach ($variables['results'] as $result) {
        if ($result['node']->type == $node_type->type) $nbresults++;
      }
      if ($nbresults) $items[] = l($node_type->name, 'search/node/' . $keys . ' type:' . $node_type->type);
    }
    if (count($items) > 2) $variables['filter'] = theme('item_list', array('items' => $items, 'title' => $variables['filter-title']));
  }
  
}

/*
 * Customisation of the results info.
 */
function voicebase_preprocess_search_result(&$variables) {
  // used to identify the correct info string
  $comment_str = preg_replace("/[0-9] (\b[a-z]*\b).*/", "$1", t('1 comment'));
  $path = explode('/', $_GET['q'], 3);
  $keys = empty($_REQUEST['keys']) ? '' : $_REQUEST['keys'];
  if (count($path) == 3) $keys = $path[2];
  if (strpos($keys, 'type:') !== FALSE) {
    $keys = drupal_substr($keys, 0, strpos($keys, 'type:')-1);
  }
  
  if (voicebase_check_type($variables["result"]["node"] -> type)) {
    $variables['keys'] = $keys;
  } else {
    $variables['keys'] = "";
  }
  $infos = array();
  if (isset($variables['info_split'])) {
    foreach ($variables['info_split'] as $key => $info) {
      if (!is_numeric($key)) {
        array_push($infos, $info);
      }
      else {
        array_push($infos, $info);
      }
    }
  }
  $variables['info'] = implode(' - ', $infos);
}